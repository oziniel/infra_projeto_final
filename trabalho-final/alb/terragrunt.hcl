include {
  path = find_in_parent_folders()
}

dependency "eks" {  

  config_path = "../terragrunt-eks"
  
}

inputs = {
  cluster_name = dependency.eks.outputs.cluster_id  
  cluster_oidc_issuer_url = dependency.eks.outputs.cluster_oidc_issuer_url                                        
}
terraform {
  source = "git::git@gitdev.clarobrasil.mobi:devops/manchestergit/terraform/modules/eks-alb.git?ref=master"
  extra_arguments "custom_vars" {
    commands = [
        "apply",
        "plan",
        "destroy",
        "import",
        "push",
        "refresh"
    ]
  }
}