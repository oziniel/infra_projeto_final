remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite"
  }
  config = {
    bucket         = "claro-terraform-tfstate-storage-projetofinal-dupla06"
    key = "dev/${path_relative_to_include()}/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform-state-lock"
  }
}
inputs = {
   aws_region = "us-east-1"
   project_name = "dev-trilha"
   env = "dev"
   tags = {
     ambiente        = "dev"
     torre           = "digital"
     centro-de-custo = "ccoe"
     projeto         = "dev-trilha"
     conta           = "dev-trilha"
     plataforma      = "aws"
   }
}
generate "provider" {
    path      = "provider.tf"
    if_exists = "overwrite"
    contents = <<EOF
provider "aws" {
  region    = "us-east-1"
  profile   = "dev-trilha"
}
provider "aws" {
  region    = "us-east-1"
  alias     = "dns"
  profile   = "prd-ongoing"
}
EOF
}