include {
  path = find_in_parent_folders()
}

inputs = {
  cluster_name    = "projetofinal-D6"
  cluster_version = "1.23"
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true
  cluster_addons = {
    aws-ebs-csi-driver = {
      resolve_conflicts = "OVERWRITE"
      service_account_role_arn = "arn:aws:iam::461872781951:role/ebs-csi-projetofinal-D6"
  }
}
  vpc_id     = "vpc-09b7d44643363040f"
  subnet_ids = ["subnet-010bd5c0f05bd084a","subnet-094205ab994a14c06"]
  eks_managed_node_groups = {                                                
    projetofinal-nodes = {
      min_size               = 1
      max_size               = 2
      desired_size           = 1
      create_launch_template = false                                                   
      launch_template_name   = ""
      disk_size              = 50
      key_name               = "projetofinal"
      ami_type               = "AL2_x86_64"
      instance_types         = ["t3.large"]
      capacity_type          = "ON_DEMAND"
    }
  }
create_aws_auth_configmap = true
manage_aws_auth_configmap = true

aws_auth_roles = [
  {
    rolearn  = "arn:aws:iam::461872781951:role/jenkins-cross-account-role"
    username = "admin"
    groups   = ["system:masters"]
  },
  {
    rolearn  = "arn:aws:iam::461872781951:role/aws-reserved/sso.amazonaws.com/AWSReservedSSO_PowerUserAccess_e7edc8508260758e"
    username = "admin"
    groups   = ["system:masters"]
  }
]

    tags = {
      Name            = "projetofinal-D6"
      ambiente        = "dev"
      centro-de-custo = "Ccoe"
      projeto         = "trilha"
      conta           = "dev-trilha"
      plataforma      = "aws"
      provedor        = "aws"
    }
}
terraform {
  source = "git::git@gitdev.clarobrasil.mobi:devops/manchestergit/terraform/modules/eks.git//module?ref=v1.1"
  extra_arguments "custom_vars" {
    commands = [
        "apply",
        "plan",
        "destroy",
        "import",
        "push",
        "refresh"
    ]
  }
}
