def clone_repo (){
                    echo 'Clonando Repositorio'
                    dir('trilha-ccoe-app-frontend-sample') {
                        gitCheckout(
                            branch: "master",
                            user: "ssh-gitlab-credentials",
                            git_url: "git@gitdev.clarobrasil.mobi:devops/devops-tracking/trilha-ccoe-app-frontend-sample.git"
                        )
                    }
                    sh 'cp sonar.properties ./trilha-ccoe-app-frontend-sample'
}

def teste_qualidade (){
        withSonarQubeEnv("Sonar-AWS") {
                        echo "Run sonar scan"
                        sh """docker run --rm \
                            -v ${workspace}/trilha-ccoe-app-frontend-sample:/usr/src \
                            -w /usr/src \
                            579496167651.dkr.ecr.us-east-1.amazonaws.com/base-images-claro/sonar-scanner \
                            /node_modules/sonar-scanner/bin/sonar-scanner -Dproject.settings=/usr/src/sonar.properties"""
                    }
                    echo "QUALITY GATE"
                    timeout(time: 60, unit: 'MINUTES') {
                        def qg = waitForQualityGate()
                        if (qg.status != 'OK') {
                            echo "O CÓDIGO FOI REPROVADO NO QUALITY GATE. DEVE-SE CORRIGIR PARA PROSSEGUIR O PIPELINE"
                        }
                    }
}

def build_imagem(){
        script {
                    sh "echo VITE_API_SERVER='https://projeto-final-grupo06-back.clarobrasil.mobi' > ./trilha-ccoe-app-frontend-sample/.env" 
                    sh "docker build ./trilha-ccoe-app-frontend-sample \
                    -t ${repositorio_ecr}:${version}"
                }
}

def enviando_ecr(){
        script {
                    echo 'Logando ECR...'
                    sh """eval \$(aws ecr get-login --no-include-email --profile $aws_profile --region $aws_region)"""
                    echo 'Tagging image to latest'
                    sh "docker tag ${repositorio_ecr}:${version} ${repositorio_ecr}:latest"

                    echo 'Upload docker images to AWS ECR'
                    sh "docker push ${repositorio_ecr}:latest"
                    sh "docker push ${repositorio_ecr}:${version}"

                    echo 'Removendo imagem Docker'
                    sh "docker image rm ${repositorio_ecr}:${version}"
                    sh "docker image rm ${repositorio_ecr}:latest"
                }
}

def deploy_eks(){
            script {
                    echo 'Autenticando no EKS'
                    sh "aws eks update-kubeconfig --name ${cluster_name} --profile $aws_profile --region $aws_region"

                    echo 'Atualizar imagem da aplicação dentro do EKS...'
                    sh "kubectl set image deployment/frontend frontend=${repositorio_ecr}:latest; "

                    echo 'Rollout da aplicação no EKS...'
                    sh "kubectl rollout status deployment/frontend --timeout=300s;"
                }

}

return this

  
