def buid_app_back (){
    script{
        echo 'Cloning BackEnd App Repo'
        dir('trilha-ccoe-app-backend-sample') {
            gitCheckout(
                branch: "master",
                user: "ssh-gitlab-credentials",
                git_url: "git@gitdev.clarobrasil.mobi:devops/devops-tracking/trilha-ccoe-app-backend-sample.git"
            )
        }
        sh 'pwd'
        sh 'ls -la'
        sh 'cp sonar.properties ./trilha-ccoe-app-backend-sample'
        echo 'Container maven para pasta target do backend'
        sh '''docker run --rm \
        -v $WORKSPACE/trilha-ccoe-app-backend-sample:/usr/src \
        -w /usr/src \
        maven:3.8-openjdk-11 \
        sh -c "ls -la && mvn clean package" '''//executar o container com seg comandos" -c "
        sh 'ls -la' // verificando arquivos
        sh 'ls -la ./trilha-ccoe-app-backend-sample/target/'//confirmando criação dos arquivos dentro da pasta "target"
    }       
}

def sonarqube (){
    script {
        withSonarQubeEnv("Sonar-AWS") {
            echo "Auth ECR"
            sh """eval \$(aws ecr get-login --no-include-email --profile prd-ongoing --region us-east-1)"""
            echo "Run sonar scan"
            sh """docker run --rm \
                -v ${workspace}/trilha-ccoe-app-backend-sample:/usr/src \
                -w /usr/src \
                579496167651.dkr.ecr.us-east-1.amazonaws.com/base-images-claro/sonar-scanner \
                /node_modules/sonar-scanner/bin/sonar-scanner -Dproject.settings=/usr/src/sonar.properties"""   
        }
        echo "QUALITY GATE"
        timeout(time: 60, unit: 'MINUTES') {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
                echo "O CÓDIGO FOI REPROVADO NO QUALITY GATE. DEVE-SE CORRIGIR PARA PROSSEGUIR O PIPELINE"
                input 'CÓDIGO REPROVADO NO QUALITY GATE! DESEJA CONTINUAR?'
            }
        }
    }
}
def build_Image (){
    script {
        echo 'Build image dockerfile'
        sh "docker build ./trilha-ccoe-app-backend-sample \
        -t ${container_name}:${version}"
    }
}

def push_ecr (){
    script {
        echo 'Auth ECR...'
        sh """eval \$(aws ecr get-login --no-include-email --profile $aws_profile --region $aws_region)"""
        echo 'Tagging image to latest'
        sh "docker tag ${container_name}:${version} ${container_name}:latest"

        echo 'Upload docker images to AWS registry'
        sh "docker push ${container_name}:latest"
        sh "docker push ${container_name}:${version}"

        echo 'Cleaning image docker...'
        sh "docker image rm ${container_name}:${version}"
        sh "docker image rm ${container_name}:latest"
    }
}

def deploy_eks (){
    script {
        echo 'Autenticando no EKS'
        sh "aws eks update-kubeconfig --name ${cluster_name} --profile $aws_profile --region $aws_region"

        echo 'Atualizar imagem da aplicação dentro do EKS...'// atualização da aplicação
        sh "kubectl set image deployment/backend backend=${container_name}:latest; "

        echo 'Rollout da aplicação no EKS...'// gerenciar a implantação da atualização
        sh "kubectl rollout status deployment/backend --timeout=30s;"//inspencionando
    }
}

return this         
        
